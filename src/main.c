#include <stdint.h>

#include "driver/gpio.h"
#include "esp32/rom/ets_sys.h"
#include "esp_task_wdt.h"
#include "esp_system.h"

void sleep_ms(uint32_t ms) {
	TickType_t delay = ms / portTICK_PERIOD_MS;
	vTaskDelay(delay);
}

uint32_t rand_secs(uint32_t min, uint32_t max) {
	const uint32_t value = esp_random() % max;
	return value > min ? value : min;
}

void app_main() {
	for (;;) {
		printf("Random value: %u\n", rand_secs(100, 500));
		sleep_ms(500);
	}
}

